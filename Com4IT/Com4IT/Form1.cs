﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Com4IT
{
    public partial class Form1 : Form
    {
        private static List<Dokument> lista;
        public Form1()
        {
            InitializeComponent();
            label2.Visible = false;
            textBox1.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            dateTimePicker1.Visible = false;
            dateTimePicker2.Visible = false;
            button9.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            listView1.View = View.Details;
            listView1.Columns.Add("Numer dokumentu");
            listView1.Columns.Add("Data dokumentu");
            listView1.Columns.Add("Pozycje dokumentu");
            lista = new List<Dokument>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "klucz1 wartość1, klucz2 wartość2, ...";
            textBox1.Click += new System.EventHandler(this.textBox1_Click);
            label2.Visible = true;
            textBox1.Visible = true;
            button5.Text = "Zapisz";
            button5.Visible = true;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lista = new List<Dokument>(IO.WczytajDane());
            WypelnijListView(lista);
        }

        private void WypelnijListView(List<Dokument> lista)
        {
            listView1.Items.Clear();
            StringBuilder temp = new StringBuilder();
            foreach (Dokument item in lista)
            {
                temp.Clear();
                ListViewItem wiersz = new ListViewItem(item.numerDokumentu);
                ListViewItem.ListViewSubItem data = new ListViewItem.ListViewSubItem(wiersz, item.dataDokumentu.ToString());
                wiersz.SubItems.Add(data);
                foreach (var item2 in item.pozycjeDokumentu)
                {
                    temp.Append(item2.Key + " " + item2.Value + ", ");
                }
                ListViewItem.ListViewSubItem pozycje = new ListViewItem.ListViewSubItem(wiersz, temp.ToString());
                wiersz.SubItems.Add(pozycje);
                listView1.Items.Add(wiersz);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Click -= textBox1_Click;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Dokument dokument = new Dokument(textBox1.Text);
            lista.Add(dokument);
            IO.ZapiszDane(lista);
            label2.Visible = false;
            textBox1.Visible = false;
            button5.Visible = false;
            WypelnijListView(lista);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var linqCollection = from dokument in lista
                                 where dokument.numerDokumentu == listView1.SelectedItems[0].Text
                                 select dokument; //moja pierwsza linijka napisana pod LINQ! :-)
            lista.Remove(linqCollection.ToList<Dokument>()[0]);
            IO.ZapiszDane(lista);
            WypelnijListView(lista);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            button7.Visible = true;
            textBox1.Text = "Wpisz nazwę poszukiwanej pozycji i kliknij Szukaj. Po zakończeniu, kliknij Zakończ.";
            textBox1.Click += new System.EventHandler(this.textBox1_Click);
            textBox1.Visible = true;
            button6.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            WypelnijListView(lista);
            button6.Visible = false;
            textBox1.Visible = false;
            button7.Visible = false;
            dateTimePicker1.Visible = false;
            dateTimePicker2.Visible = false;
            button9.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var linqCollection = from dokument in lista
                                 where dokument.pozycjeDokumentu.ContainsKey(textBox1.Text)
                                 select dokument;
            List<Dokument> wynik = linqCollection.ToList<Dokument>();
            WypelnijListView(wynik);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            button6.Visible = true;
            button9.Visible = true;
            dateTimePicker1.Visible = true;
            dateTimePicker2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = dateTimePicker1.Value.AddHours((dateTimePicker1.Value.Hour * (-1)));
            dateTimePicker1.Value = dateTimePicker1.Value.AddMinutes((dateTimePicker1.Value.Minute * (-1)));
            dateTimePicker2.Value = dateTimePicker2.Value.AddHours((23 - dateTimePicker2.Value.Hour));
            dateTimePicker2.Value = dateTimePicker2.Value.AddMinutes((59 - dateTimePicker2.Value.Minute));
            var linqCollection = from dokument in lista
                                 where DateTime.Compare(dokument.dataDokumentu, dateTimePicker1.Value) >= 0
                                 && DateTime.Compare(dokument.dataDokumentu, dateTimePicker2.Value) <= 0
                                 select dokument; 
            List<Dokument> wynik = linqCollection.ToList<Dokument>();
            WypelnijListView(wynik);
        }

    }
}
