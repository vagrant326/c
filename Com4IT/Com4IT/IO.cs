﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Com4IT
{
    class IO
    {
        private static string adres = (Environment.CurrentDirectory + "\\baza.txt");

        public static void ZapiszDane(List<Dokument> lista)
        {
            StreamWriter writer = new StreamWriter(adres);
            foreach (Dokument item in lista)
            {
                ZapiszDokument(item,writer);
            }
            writer.Close();
        }

        private static void ZapiszDokument(Dokument dokument, StreamWriter writer)
        {
            writer.WriteLine(dokument.numerDokumentu);
            writer.WriteLine(dokument.dataDokumentu.ToString());
            foreach (KeyValuePair<string,int> item in dokument.pozycjeDokumentu)
            {
                writer.WriteLine(item.Key + " " + item.Value);
            }
            writer.WriteLine("^^^");
        }

        public static List<Dokument> WczytajDane()
        {
            List<Dokument> lista = new List<Dokument>();
            string linia;
            List<string> dane = new List<string>();
            if (File.Exists(adres))
            {
                StreamReader reader = new StreamReader(adres);
                while ((linia=reader.ReadLine())!=null)
                {
                    dane.Clear();
                    dane.Add(linia);
                    while ((linia=reader.ReadLine())!="^^^")
                    {
                        dane.Add(linia);
                    }
                    lista.Add(new Dokument(dane));
                }
                reader.Close();
            }
            return lista;
        }
    }
}
