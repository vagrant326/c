﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com4IT
{
    class Dokument
    {
        private static int[] dane = Baza.WczytajKonfiguracje();

        private static int ostatniNumer = dane[0];
        private static int ostatniMiesiąc = dane[1];
        private static int ostatniRok = dane[2];

        private int numer;
        public DateTime dataDokumentu;
        public Dictionary<string, int> pozycjeDokumentu;

        public string numerDokumentu
        {
            get
            {
                return (numer.ToString() + "/" + dataDokumentu.Month.ToString() + "/" + dataDokumentu.Year.ToString());
            }
        }

        public Dokument()
        {
            dataDokumentu = DateTime.Now;
            pozycjeDokumentu = new Dictionary<string, int>();

            if (czyZerować())
            {
                numer = 1;
            }
            else
            {
                numer = (ostatniNumer + 1);
            }
            Baza.ZapiszKonfiguracje(new int[] { numer, dataDokumentu.Month, dataDokumentu.Year });
            ostatniNumer = numer;
            ostatniMiesiąc = dataDokumentu.Month;
            ostatniRok = dataDokumentu.Year;
        }

        public Dokument(List<string> lista)
        {
            string numerDokumentu = lista[0];
            string[] temp = numerDokumentu.Split(new char[]{'/'},StringSplitOptions.RemoveEmptyEntries);
            numer = Convert.ToInt32(temp[0]);
            temp = lista[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string[] tempData = temp[0].Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            int rok = Convert.ToInt32(tempData[0]);
            int miesiąc = Convert.ToInt32(tempData[1]);
            int dzień = Convert.ToInt32(tempData[2]);
            string[] tempGodzina = temp[1].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            int godzina = Convert.ToInt32(tempGodzina[0]);
            int minuta = Convert.ToInt32(tempGodzina[1]);
            int sekunda = Convert.ToInt32(tempGodzina[2]);
            DateTime data = new DateTime(rok,miesiąc,dzień,godzina,minuta,sekunda);
            dataDokumentu = data;
            pozycjeDokumentu = new Dictionary<string, int>();
            for (int i = 2; i < lista.Count; i++)
            {
                temp = lista[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                int wartosc = Convert.ToInt32(temp[1]);
                pozycjeDokumentu.Add(temp[0], wartosc);
            }

        }

        public Dokument(string pozycje):this()
        {
            string[] temp = pozycje.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] temp2;
            int wartosc;
            foreach (string item in temp)
            {
                temp2 = item.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                wartosc = Convert.ToInt32(temp2[1]);
                pozycjeDokumentu.Add(temp2[0], wartosc);
            }
        }
        
        private bool czyZerować()
        {
            DateTime dziś = new DateTime();
            dziś = DateTime.Now;
            bool odpowiedz = true;
            if (ostatniRok == dziś.Year)
            {
                if (ostatniMiesiąc == dziś.Month)
                {
                    odpowiedz = false;
                }
            }
            return odpowiedz;
        }

    }
}
