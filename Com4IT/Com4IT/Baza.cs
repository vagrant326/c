﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Com4IT
{
    class Baza
    {
        #region Klucz zapisu configa
        /*
         * 1. Numer ostatniego dokumentu
         * 2. Miesiąc utworzenia ost. dok.
         * 3. Rok utworzenia ost. dok.
        */
        #endregion

        private static string adres = (Environment.CurrentDirectory + "\\cfg.ini");

        public static int[] WczytajKonfiguracje()
        {
            string[] linie;
            int[] konfiguracja = new int[3];
            if(File.Exists(adres))
            {
                linie = System.IO.File.ReadAllLines(adres);
                if (linie.GetLength(0)==0)
                {
                    throw new Exception("cfg istnieje, ale jest pusty");
                }
                for (int i = 0; i < konfiguracja.GetLength(0); i++)
                {
                    konfiguracja[i] = Convert.ToInt32(linie[i]);
                }
            }
            else
            {
                FileStream plik = File.Create(adres);
                plik.Close();
                konfiguracja = new int[] { 0, DateTime.Now.Month, DateTime.Now.Year };
            }
            return konfiguracja;
        }

        public static void ZapiszKonfiguracje(int[] dane)
        {
            string[] linie = new string[dane.GetLength(0)];
            for (int i = 0; i < dane.GetLength(0); i++)
            {
                linie[i] = dane[i].ToString();
            }
            File.WriteAllLines(adres, linie);
        }
    }
}
