﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        Graphics uklad,wykres;
        Pen olowek, czerwony;
        public Form1()
        {
            InitializeComponent();
            uklad = panel1.CreateGraphics();
            wykres = panel1.CreateGraphics();
            olowek=new Pen(Color.Black,2);
            czerwony=new Pen(Color.Red,3);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            uklad.DrawLine(olowek, 0, panel1.Height / 2, panel1.Width, panel1.Height / 2);
            uklad.DrawLine(olowek, panel1.Width / 2, 0, panel1.Width/2, panel1.Height);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(textBox1.Text);
            double b = Convert.ToDouble(textBox2.Text);
            double c = Convert.ToDouble(textBox3.Text);
            double d = Convert.ToDouble(textBox4.Text);
            double wynikx, wyniky;
            int skala = panel1.Width / 20;
            double staryx = -10, staryy, nowyx, nowyy;
            staryy = ((a * staryx * staryx * staryx) + (b * staryx * staryx) + (c * staryx) + d) * skala;
            for (double i = -10; i <=  10; i+=0.001)
            {
                wynikx = i;
                wyniky=(a * wynikx * wynikx * wynikx) + (b * wynikx * wynikx) + (c * wynikx) + d;
                nowyx = wynikx * skala;
                nowyy = wyniky * skala;
                wykres.DrawLine(czerwony, (int)(staryx) + panel1.Width / 2, (int)(-staryy) + panel1.Height / 2, (int)(nowyx) + panel1.Width / 2, (int)(-nowyy) + panel1.Height / 2);
                staryx = nowyx;
                staryy = nowyy;
            }

        }
    }
}
