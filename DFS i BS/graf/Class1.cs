﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace graf
{
    public abstract class BaseSearchClass
    {
        public List<int> open_list { get; set; }
        public List<int> closed_list { get; set; }

        public abstract void AddNodes(List<int> childs);

        public BaseSearchClass()
        {

        }

        public void SearchNode(int id, List<List<int>> graph)
        {
            open_list = new List<int>();

            closed_list = new List<int>();

            open_list.Add(0);

            while (open_list.Any())
            {
                var node = open_list.First();
                Console.WriteLine("Wezel " + node);
                if (node == id)
                {
                    Console.WriteLine("Zakonczono przeszukiwanie grafu.");
                    break;
                }
                closed_list.Add(node);
                open_list.RemoveAt(0);

                var children = new List<int>(graph[node]);
                AddNodes(children);
            }
        }
    }

    public class DFSSearch : BaseSearchClass
    {
        public override void AddNodes(List<int> childs)
        {
            open_list.InsertRange(0, childs);
        }
    }

    public class BSearch : BaseSearchClass
    {
        public override void AddNodes(List<int> childs)
        {
            open_list.AddRange(childs);
        }
    }

}
