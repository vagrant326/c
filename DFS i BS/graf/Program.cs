﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace graf
{
    class Program
    {
        static void Wypelnij(out List<List<int>> lista)
        {
            lista = new List<List<int>>();
            StreamReader SRd = new StreamReader("graf.txt");
            string linia;
            List<string> linie = new List<string>();
            int n = Convert.ToInt32(SRd.ReadLine());
            char[] spacja = new char[1];
            spacja[0] = ' ';
            while ((linia=SRd.ReadLine())!=null)
            {
                linie.Add(linia);                
            }
            for (int i = 0; i < n; i++)
            {
                lista.Add(new List<int>());
            }
            foreach (string item in linie)
            {
                string[] temp = item.Split(spacja,StringSplitOptions.RemoveEmptyEntries);
                int z_ = Convert.ToInt32(temp[0]);
                int _do = Convert.ToInt32(temp[1]);
                lista[z_].Add(_do);
            }
        }

        static void Main(string[] args)
        {
            List<List<int>> sasiedzi;
            int szukany;
            Console.Write("Podaj wierzcholek poszukiwany: ");
            szukany = Convert.ToInt32(Console.ReadLine());
            Wypelnij(out sasiedzi);
            BSearch instancjaBS = new BSearch();
            DFSSearch instancjaDFS = new DFSSearch();
            Console.WriteLine("Wszerz:");
            instancjaBS.SearchNode(szukany, sasiedzi);
            Console.WriteLine();
            Console.WriteLine("Wglab:");
            instancjaDFS.SearchNode(szukany, sasiedzi);
            Console.WriteLine();
            Console.Read();
        }
    }
}
