﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Heurystyka
{
    class Program
    {
        public class Node : IComparable<Node>
        {
            public int id;
            public int wartosc;
            public int f_oceny;
            public int CompareTo(Node other)
            {
                if (this.f_oceny>other.f_oceny)
                {
                    return 1;
                }
                else
                {
                    if (this.f_oceny==other.f_oceny)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
            public Node(int i, int w)
            {
                id = i;
                wartosc = w;
            }
        }
        static int[][] WypelnijMacierz()
        {
            StreamReader SRd = new StreamReader("macierz.txt");
            List<string> linie = new List<string>();
            string[] spacja = new string[1];
            spacja[0]=" ";
            string linia;
            while ((linia=SRd.ReadLine())!=null)
	        {
                linie.Add(linia);
	        }
            int[][] macierz = new int[linie.Count][];
            for (int i = 0; i < macierz.GetLength(0); i++)
            {
                macierz[i] = new int[linie.Count];
            }
            for (int i = 0; i < linie.Count; i++)
            {
                string[] temp = linie[i].Split(spacja, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < linie.Count; j++)
                {
                    macierz[i][j] = Convert.ToInt32(temp[j]);
                }
            }
            return macierz;
        }

        static List<List<Node>> StworzGraf(int[][] macierz)
        {
            List<List<Node>> lista = new List<List<Node>>();
            for (int i = 0; i < macierz.GetLength(0); i++)
            {
                lista.Add(new List<Node>());
            }
            for (int i = 0; i < macierz.GetLength(0); i++)
            {
                for (int j = 0; j < macierz.GetLength(0); j++)
                {
                    if (macierz[i][j]!=0)
                    {
                        Node temp = new Node(j,macierz[i][j]);
                        lista[i].Add(temp);
                    }
                }
            }
            return lista;
        }

        public static void SearchNode(int id, List<List<Node>> graph)
        {
            List<Node> open_list = new List<Node>();
            List<Node> closed_list = new List<Node>();
            Node zero = new Node(0, 0);
            open_list.Add(zero);
            while (open_list.Any())
            {
                var node = open_list.First();
                Console.WriteLine("Wezel " + node.id + ", funkcja oceny = " + node.f_oceny);
                if (node.id == id)
                {
                    Console.WriteLine("Zakonczono przeszukiwanie grafu.");
                    break;
                }
                closed_list.Add(node);
                open_list.RemoveAt(0);

                var children = new List<Node>(graph[node.id]);
                AddNodes(children, node, ref open_list);
            }
        }

        public static void AddNodes(List<Node> lista, Node parent, ref List<Node> ol)
        {
            foreach (Node item in lista)
            {
                item.f_oceny = item.wartosc + parent.f_oceny;
                ol.Insert(0, item);
            }
            ol.Sort();
        }
        static void Main(string[] args)
        {
            int[][] macierz = WypelnijMacierz();
            List<List<Node>> graf = StworzGraf(macierz);
            Console.Write("Wpisz poszukiwany wierzcholek: ");
            int szukany = Convert.ToInt32(Console.ReadLine());
            SearchNode(szukany, graf);
            Console.Read();
        }
    }
}
